import torch
from torch import nn

class Network(nn.Module):
    def __init__(self):
        super().__init__()
        
        self.hidden = nn.Linear(300, 64)
        self.output = nn.Linear(64, 3)
        
        self.sigmoid = nn.Sigmoid()
        self.softmax = nn.Softmax(dim=1)
        
    def forward(self, x):
        x = self.hidden(x)
        x = self.sigmoid(x)
        x = self.output(x)
        x = self.softmax(x)
        
        return x

    def predict(self, pic):
        device = "cpu"
        data = torch.stack(pic).to(device)
        x = self.hidden(data)
        x = self.sigmoid(x)
        x = self.output(x)
        x = self.softmax(x)
        _, predicted = torch.max(x, 1)
        return predicted