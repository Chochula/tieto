from camera import take_picture, preprocess_image
from network import Network
import torch
import os

labels = ["O", "X", "other"]

try:
    img_name = take_picture()

    path = "data/"
    image = preprocess_image(os.path.join(path , img_name))

    model = Network()
    PATH = "data/model"
    model.load_state_dict(torch.load(PATH))
    model.eval()
    predicted = model.predict(image)
    print(f"My guess is that this image belongs to class '{labels[predicted.item()]}'")

except Exception as e:
    print(e)
