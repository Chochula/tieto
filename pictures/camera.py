import cv2
import os
import torch

def take_picture():
    try:
        cam = cv2.VideoCapture(0)

        cv2.namedWindow("test")

        img_counter = 0

        print('Press space to capture image using your front camera')
        while True:
            ret, frame = cam.read()
            if not ret:
                print("failed to grab frame")
                break
            cv2.imshow("test", frame)

            k = cv2.waitKey(1)
            if k%256 == 27:
                break
            elif k%256 == 32:
                img_name = "frame-{}.png".format(img_counter)
                path = "data/"
                cv2.imwrite(os.path.join(path , img_name) , frame)
                print("Image - {} taken".format(img_name))
                img_counter += 1
                break

        cam.release()

        cv2.destroyAllWindows()

        return img_name
    except:
        return Exception("There was a problem. I am unable to capture the images")

def preprocess_image(path):
    image = cv2.imread(path, cv2.IMREAD_UNCHANGED)
    B, G, R = cv2.split(image)
    image = cv2.merge((B, G, R))
    image = cv2.resize(image, (10,10))
    return [torch.flatten(torch.from_numpy(image)).type(torch.FloatTensor)]