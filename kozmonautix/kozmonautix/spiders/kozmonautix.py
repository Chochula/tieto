# -*- coding: utf-8 -*-
import scrapy
import re

from kozmonautix.items import KozmonautixItem


class KozmonautixSpider(scrapy.Spider):
    name = 'kozmonautix'
    allowed_domains = ['kosmonautix.cz']
    start_urls = [
        'https://kosmonautix.cz/']

    custom_settings = {
        'FEED_EXPORT_ENCODING': 'utf-8'
    }

    def parse_article(self, response):
        item = KozmonautixItem()

        metadata = response.xpath('//div[@id="content"]//div[@class="postdate"]//text()').getall()
        author = metadata[2].strip()
        date = metadata[0].strip()
        title = response.xpath('//h2[@class="title"]/text()').get().replace('\xa0', ' ')
        text = response.xpath('//div[@class="entry"]//p[not(em[contains(text(), "Zdroje obrázků:")]) and not(em[contains(text(), "Přeloženo z:")]) and not(em[contains(text(), "Zdroje informací:")]) and not(ancestor::div[contains(@id, "attachment")])]//text()').getall()

        item['title'] = title
        item['date_of_creation'] = date
        item['author'] = author
        text = [t.replace('\xa0', ' ') for t in text]
        item['article'] = ' '.join(text)

        yield item

    def parse_articles(self, response):
        articles_url = response.xpath('//div[@id="content"]//h2//a/@href').getall()
        for url in articles_url: 
            yield scrapy.Request(url=url, callback=self.parse_article)

    def parse(self, response):
        try:
            num_last_page = int(response.xpath('//div[@class="wp-pagenavi"]//a//text()').getall()[-1])
            base_url = "https://kosmonautix.cz/page/"
            for i in range(1, num_last_page+1):
                url = base_url + str(i)
                yield scrapy.Request(url=url, callback=self.parse_articles)

        except Exception as e:
            print(e)
