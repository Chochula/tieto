# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class KozmonautixItem(scrapy.Item):
    title = scrapy.Field()
    author = scrapy.Field()
    date_of_creation = scrapy.Field()
    article = scrapy.Field()
