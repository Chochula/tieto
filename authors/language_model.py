import fasttext
import fasttext.util

class LanguageModel():

    def __init__(self, path):
        self.model = fasttext.load_model(path)
        self.lang_size = set()

    def get_word_vector(self, word):
        return self.model.get_word_vector(word)
    
    def extend_lang_size_by_list(self, words):
        for word in words:
            self.lang_size.add(word)

    def extend_lang_size_by_word(self, word):
        self.lang_size.add(word)

    def get_lang_size(self):
        return len(self.lang_size)
    