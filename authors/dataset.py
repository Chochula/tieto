import pandas as pd
import json
import torch

class Dataset():

    def __init__(self, path):
        self.df = json.load(open(path))
        self.df = pd.json_normalize(self.df)

    def preprocess(self, lang_model):
        PAD = "<PAD_TOKEN>"

        majer = 0
        self.df = self.df.drop(self.df.columns[[0, 1]], axis=1)
        authors = ['Dušan Majer', 'Lukáš Houška', 'Ondřej Šamárek', 'Michael Voplatka', 'Tomáš Kohout']
        for index, row in self.df.iterrows():
            
            if row['author'] not in authors:
                row['author'] = "Others"
            
            if row['author'] == "Dušan Majer":
                if majer < 500:
                    majer +=1
                else: 
                    row['author'] = "X"

        self.df = self.df[self.df['author'] != "X"]
        self.df['article'] = self.df['article'].apply(lambda x: self.pad(x.split()[:60], 60))
        self.df['article'] = self.df['article'].apply(lambda x: self.convert_to_vectors(x, lang_model))
        self.df['author'] = self.df['author'].apply(lambda x: self.author_to_idx(x))

        self.df = self.df.sample(frac=1).reset_index(drop=True)


    def author_to_idx(self, author):
        authors = ['Dušan Majer', 'Lukáš Houška', 'Ondřej Šamárek', 'Michael Voplatka', 'Tomáš Kohout', "Others"]
        return torch.tensor([authors.index(author)], dtype=torch.long)

    def idx_to_author(self, idx):
        authors = ['Dušan Majer', 'Lukáš Houška', 'Ondřej Šamárek', 'Michael Voplatka', 'Tomáš Kohout', "Others"]
        return authors[idx]

    def convert_to_vectors(self, list_words, lang_model):
        tensor = torch.zeros(len(list_words), 1, 300)
        for i, word in enumerate(list_words):
            tensor[i][0] = torch.FloatTensor(lang_model.get_word_vector(word)).type(torch.FloatTensor)

        return tensor

    def get_train_test(self):
        train_X, train_Y = self.create_data(self.df.head(4000))
        test_X, test_Y = self.create_data(self.df.tail(len(self.df) - 4000))

        return train_X, train_Y, test_X, test_Y

    def create_data(self, d):
        authors = list()
        articles = list()
        for index, row in d.iterrows():
            articles.append(row['article'])
            authors.append(row['author'])
        
        return articles, authors

    def pad(self, some_list, target_len):
        PAD = "<PAD_TOKEN>"
        if len(some_list) < 100: 
            num = target_len - len(some_list)
            for i in range(num):
                some_list.append(PAD)
        return some_list
