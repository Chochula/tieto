import torch
import torch.nn as nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence
import torch.optim as optim


class LSTM(nn.ModuleList):

	def __init__(self, hidden_dim = 300, layers=1, input_size = 300, output_size = 6):
		super(LSTM, self).__init__()
		
		self.hidden_dim = hidden_dim
		self.LSTM_layers = layers
		self.input_size = input_size
		self.out_size = output_size
		
		self.dropout = nn.Dropout(0.2)
		self.lstm = nn.LSTM(input_size=self.hidden_dim, hidden_size=self.hidden_dim, num_layers=self.LSTM_layers)
		self.fc = nn.Linear(self.hidden_dim, self.out_size)
		self.softmax = nn.Softmax(dim=1)
		
	def forward(self, x, hidden, cell):

		out, (hidden, cell) = self.lstm(x.unsqueeze(0), (hidden, cell))
		out = self.fc(out.reshape(out.shape[0], -1))
		
		return out, hidden, cell
	
	def init_hidden(self, batch_size, num_inputs, input_size):
		hidden = torch.zeros(batch_size, num_inputs, input_size)
		cell = torch.zeros(batch_size, num_inputs, input_size)

		return hidden, cell