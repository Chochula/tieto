
from language_model import LanguageModel
from dataset import Dataset
from network import LSTM
import torch
from torch import nn

PATH_DATASET = "./data/kozmonautix.json"
PATH_MODEL = "./data/cc.cs.300.bin"

lang_model = LanguageModel(PATH_MODEL)

dataset = Dataset(PATH_DATASET)
dataset.preprocess(lang_model)
train_X, train_Y, test_X, test_Y = dataset.get_train_test()

model = LSTM().to('cpu')

epochs = 100
print_every = 1
optimizer = torch.optim.Adam(model.parameters(), lr=0.003)
criterion = nn.CrossEntropyLoss()
max_pred = 0

for e in range(epochs):

    model.zero_grad()
    model.train()
    loss = 0
    
    for X, Y in zip(train_X, train_Y):
        hidden, cell = model.init_hidden(1 , 1, 300)
        X = X.to('cpu')
        Y = Y.to('cpu')
        for i in range(X.size()[0]):
            output, hidden, cell = model(X[i], hidden, cell)

        loss = criterion(output, Y)
        loss.backward()
        optimizer.step()

    if e % print_every == 0 and e != 0:
        model.eval()
        with torch.no_grad():
            
            correct = 0
            for X, Y in zip(train_X, train_Y):
                X = X.to('cpu')
                Y = Y.to('cpu')
                hidden, cell = model.init_hidden(1 , 1, 300)

                for i in range(X.size()[0]):
                    output, hidden, cell = model(X[i], hidden, cell)

                _, predicted = torch.max(output.data, 1)

                if predicted == Y:
                    correct += 1

                    if correct > max_pred:
                        max_pred = correct
                        PATH = "/home/peter/Projects/tieto/authors/data/model"
                        torch.save(model.state_dict(), PATH)

            acc = float("{:.2f}".format(correct/len(train_Y)))
            print(f"After {e} epochs got {correct} correct with acc {acc}")
